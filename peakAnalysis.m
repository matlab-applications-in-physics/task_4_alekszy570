% Matlab R2020b script
% name: peakAnalysis.m
% author: alekszy570
% date: 2020-12-06
% version: v1
% I used jakugre's code


% open and list the file
my_file = dir('Co60_1350V_x20_p7_sub.dat');
my_file_id = fopen('Co60_1350V_x20_p7_sub.dat');


% assignment value
size = 10000000; 
number = 0;

%loop counts the values ​​of pulses
for n = 1:(my_file.bytes/size)+1
    [num,edges] = histcounts(fread(my_file_id, size,'uint8'),'BinLimits',[40,70],'BinWidth',1);
    number = number + num;
end;

%display a histogram
histogram('BinEdges',edges,'BinCounts',number);
xlabel('wartosc'); ylabel('ilosc impulsow'); grid on; 
fclose(my_file_id);

